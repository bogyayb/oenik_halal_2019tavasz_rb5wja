﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace K_kozep
{
    class Klaszter
    {
        public int Kozep { get; set; }
        public List<int> Elemek { get; set; }
    }

    class Program
    {
        static Random R;

        //függvény paraméterek
        static List<int> P;

        static List<Klaszter> K;

        static int Ds(int x, int y)
        {
            return Math.Abs(P[x] - P[y]);
        }

        /// <summary>
        /// 'P' feltöltése: 200, 201, ..., 399, 400
        /// </summary>
        static void Inicializálás()
        {
            R = new Random();
            P = new List<int>();

            //feltöltés 200-tól 250-ig
            for (int i = 200; i <= 250; i++)
            {
                P.Add(i);
            }

            //kiírás
            Console.WriteLine("Keresési tér:");
            P.ForEach(x => Console.Write(x + " "));
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            Inicializálás();
            Console.WriteLine(K_kozep(3));
        }

        //algoritmus
        static string K_kozep(int N /* P, K, Ds */)
        {
            List<int> C_vesszo = InitializeCentroids(P, N);
            List<int> C;

            do
            {
                C = Masolas(C_vesszo);
                K = KlaszterekLetrehozasa(C);

                for (int i = 0; i < P.Count; i++)
                {
                    int legkozelebbiIndex = LegkozelebbiIndexSzamitas(i, C);
                    LegkozelebbibeBelerakas(i, legkozelebbiIndex);
                }

                for (int i = 0; i < C.Count; i++)
                {
                    C_vesszo[i] = KozeppontModositas(K[i]);
                }

                KlaszterekKiirasa(K); //EXTRA
            }
            while (!EgyformakE(C, C_vesszo));

            return "";
        }

        //belső függvények
        private static List<int> InitializeCentroids(List<int> P, int N)
        {
            List<int> C_vesszo = new List<int>();

            int ujC_vesszo_ertek = 0;

            for (int i = 0; i < N; i++)
            {
                do
                {
                    ujC_vesszo_ertek = R.Next(0, P.Count);
                }
                while (C_vesszo.Contains(ujC_vesszo_ertek));

                C_vesszo.Add(ujC_vesszo_ertek);
            }

            return C_vesszo;
        }

        private static List<int> Masolas(List<int> C_vesszo)
        {
            List<int> C = new List<int>();

            for (int i = 0; i < C_vesszo.Count; i++)
            {
                C.Add(C_vesszo[i]);
            }

            return C;
        }

        private static bool EgyformakE(List<int> C, List<int> C_vesszo)
        {
            for (int i = 0; i < C.Count; i++)
            {
                if (C[i] != C_vesszo[i])
                {
                    return false;
                }
            }

            return true;
        }

        private static List<Klaszter> KlaszterekLetrehozasa(List<int> C)
        {
            List<Klaszter> K = new List<Klaszter>();

            for (int i = 0; i < C.Count; i++)
            {
                K.Add(new Klaszter() { Kozep = C[i], Elemek = new List<int>() });
            }

            return K;
        }

        private static void KlaszterekKiirasa(List<Klaszter> k)
        {
            List<Klaszter> klaszter = k.OrderBy(x => x.Kozep).ToList();

            Console.WriteLine();
            foreach (var item in klaszter)
            {
                Console.Write($"Közepe: {P[item.Kozep]}; elemei: ");


                foreach (var i in item.Elemek)
                {
                    Console.Write(P[i] + ", ");
                }

                Console.WriteLine();
            }
        }

        private static int LegkozelebbiIndexSzamitas(int x, List<int> C)
        {
            int minIndex = C[0];

            foreach (int i in C)
            {
                if (Ds(x, i) < Ds(x, minIndex))
                {
                    minIndex = i;
                }
            }

            return minIndex;
        }

        private static void LegkozelebbibeBelerakas(int x, int legkozelebbiIndex)
        {
            foreach (var item in K)
            {
                if (item.Kozep == legkozelebbiIndex)
                {
                    item.Elemek.Add(x);
                    return;
                }
            }
        }

        private static int KozeppontModositas(Klaszter klaszter)
        {
            int ujKozeppont = 0;

            if (klaszter.Elemek.Count % 2 == 0)
            {
                ujKozeppont = klaszter.Elemek[(klaszter.Elemek.Count / 2)];
            }
            else
            {
                ujKozeppont = klaszter.Elemek[((klaszter.Elemek.Count - 1) / 2)];
            }

            return ujKozeppont;
        }

    }
}
