﻿using System;
using System.Collections.Generic;

namespace ReszecskeRajAlapuOptimalizalas
{
    class Allat
    {
        public int Pozicio { get; set; } //aktuális helye
        public int Sebesseg { get; set; } //aktuális sebessége

        public int opt { get; set; } //legjobb hely ahol ő járt: lokális optimum
    }

    class Program
    {
        #region paraméterek
        static List<int> S;

        private static int f(int ertek)
        {
            return S[ertek];
        }

        static bool StopCondition()
        {

            stopCounter++;
            if (stopCounter >= 100)
            {
                return true;
            }
            return false;
        }
        #endregion

        //main
        static void Main(string[] args)
        {
            Inicializálás();
            Console.WriteLine(ParticleSwarmOptimization(S, f, StopCondition));
        }

        /// <summary>
        /// P: állatok halmaza, van pozíciójuk, sebességük, és mentik a saját lok op.-ot
        /// gopt: globáblis optimum, legjobb hely ahol valaha bárki járt
        /// pvelo: 'p' elem sebessége
        /// </summary>
        static string ParticleSwarmOptimization(List<int> S, dF f, dSC StopCondition)
        {
            P = InitializePopulation(S); //léterhozunk x db állatot random induló pozícióval és random sebességgel
            Evaluatoin(P, ref g_opt, f); //fintess értékek meghatározása

            while (!StopCondition())
            {
                CalculateVelocity(P, ref g_opt);

                foreach (Allat p in P) // ∀p ∈ P
                {
                    p.Pozicio = /**/TIK(/**/ p.Pozicio + p.Sebesseg /**/)/**/; // p ← p + p_velo   //TIK = TúlIndexelés Kezelése, hibakezelés
                }

                Evaluatoin(P, ref g_opt, f);

                /**/ Kiiras(g_opt); /**/
            }

            return gpm(g_opt);
        }

        /// <summary>
        /// popt: 'p' elem saját lokális optiuma, legjobb hely ahol ő járt
        /// </summary>       
        private static void Evaluatoin(List<Allat> P, ref int g_opt, dF f)
        {
            foreach (Allat p in P) // ∀p ∈ P
            {
                if (f(p.Pozicio) < f(p.opt)) //kisebb a jobb
                {
                    p.opt = p.Pozicio; // p_opt ← p

                    if (f(p.Pozicio) < f(g_opt))
                    {
                        g_opt = p.Pozicio; // g_opt ← p
                    }
                }
            }
        }

        /// <summary>
        /// |p|: 'p' elem pozícióinak a dimneziószáma, pl. 3D-ben 3 lenne, (x, y, z)
        /// rp, rg: random számok, hogy futkorászhassanak össze-vissza
        /// ω(omega) : konstans, súlyozást segíti
        /// Φ(fí) : konstans, súlyozást segíti
        /// </summary>
        private static void CalculateVelocity(List<Allat> P, ref int g_opt)
        {
            foreach (var p in P)
            {
                for (int dimenzio = 0; dimenzio < 1; dimenzio++) //végigmegyünk a dimenziókon: pl, ha 3D-s, akkor x, y, z-re is új értéket számítunk ki, de itt most csak 1D van, tehát igazából fölösleges
                {
                    double r_p = RNDu(0, 1);
                    double r_g = RNDu(0, 1);

                    p.Sebesseg = (int)((omega * p.Sebesseg) + (fí * r_p * (p.opt - p.Pozicio)) + (fí * r_g * (g_opt - p.Pozicio)));
                }
            }
        }

        #region belső függvények
        delegate bool dSC();
        delegate int dF(int x);

        static Random R;

        static List<Allat> P;
        static int g_opt;
        static int stopCounter;
        static double fí;
        static double omega;

        static void Inicializálás()
        {
            R = new Random();
            S = new List<int>();
            stopCounter = 0;
            g_opt = 0;
            omega = 0.5;
            fí = 0.5;

            //feltöltés 49-től 1-ig
            for (int i = 50; i >= 1; i--)
            {
                S.Add(i);
            }

            //feltöltés 0-től 49-ig
            for (int i = 0; i <= 50; i++)
            {
                S.Add(i);
            }

            //feltöltés 0-től 49-ig
            for (int i = 51; i <= 100; i++)
            {
                S.Add(i);
            }

            //feltöltés 49-től 1-ig
            for (int i = 100; i >= 5; i--)
            {
                S.Add(i);
            }

            //feltöltés 0-től 49-ig
            for (int i = 5; i <= 100; i++)
            {
                S.Add(i);
            }


            //kiírás
            Console.WriteLine("Keresési tér:");
            S.ForEach(x => Console.Write(x + " "));
            Console.WriteLine();
        }

        private static List<Allat> InitializePopulation(List<int> s)
        {
            List<Allat> p = new List<Allat>();
            for (int i = 0; i < 5; i++)
            {
                p.Add(new Allat()
                {
                    Pozicio = R.Next(0, s.Count - 1),
                    Sebesseg = R.Next(1, 10)
                });
            }

            return p;
        }

        private static int TIK(int v)
        {
            return (v) >= S.Count ? S.Count - 1 : (v) < 0 ? 0 : v;
        }

        private static string gpm(int p)
        {
            return $"\nLegkisebb elem: index: {p}, érték: {S[p]}";
        }

        private static void Kiiras(int p)
        {
            Console.Write($"\nindex: {p}, érték: {S[p]}");
        }

        static double RNDu(int v1, int v2)
        {
            double r = (double)R.Next(v1, v2*100) / 100;
            return r;
        }
        #endregion
    }
}
