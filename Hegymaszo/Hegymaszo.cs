﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hegymaszo
{
    class Program
    {
        #region parméterek
        static Random R;

        //függvény paraméterei
        static List<int> S;

        static int epszilon;

        static int Ds(int p, int x)
        {
            return Math.Abs(p - x);
        }

        private static int f(int ertek)
        {
            return S[ertek];
        }

        static bool StopCondition()
        {

            stopCounter++;
            if (stopCounter >= 100)
            {
                return true;
            }
            return false;
        }

        //segéd paraméterek
        static int stopCounter;
       
        /// <summary>
        /// 'S' feltöltése: 49, 48, ..., 0, ...48, 49
        /// </summary>
        static void Inicializálás()
        {
            R = new Random();
            S = new List<int>();
            epszilon = 3;
            stopCounter = 0;

            //feltöltés 49-től 1-ig
            for (int i = 50; i >= 1; i--)
            {
                S.Add(i);
            }

            //feltöltés 0-től 49-ig
            for (int i = 0; i <= 50; i++)
            {
                S.Add(i);
            }

            //kiírás
            Console.WriteLine("Keresési tér:");
            S.ForEach(x => Console.Write(x + " "));
            Console.WriteLine();
        }
        #endregion

        static void Main(string[] args)
        {
            Inicializálás();
            Console.WriteLine(HillClimbingStochastic());
            //Console.WriteLine(HillClimbingSteepestAscent());
            //Console.WriteLine(HillClimbingRandomRestart());
        }

        //algoritmus (sztochasztikus - véletlenszerű)
        static string HillClimbingStochastic(/* S, ds, Ɛ, f, StopCondition*/)
        {
            int p = R.Next(S.Count); // p ←rnd- S

            while (!StopCondition())
            {
                int q = RandomSzamEpszilonTavolsagra(p); //q ←rnd- {x ∈ S | ds(x, p) = Ɛ}

                if (f(q) < f(p))
                {
                    p = q;
                }
                /**/ Kiiras(p); /**/
            }

            return gpm(p);
        }

        //algoritmus (meredekebb emelkedés)
        static string HillClimbingSteepestAscent(/* S, ds, Ds, Ɛ, f, StopCondition*/)
        {
            int p = R.Next(S.Count); // p ←rnd- S
            bool stuck = false;

            while (!stuck && !StopCondition())
            {
                int q = LegjobbSzamEpszilonTavolsagra(p); // q ←argmin(f)- {x ∈ S | ds(x, p) = Ɛ}

                if (f(q) < f(p))
                {
                    p = q;
                }
                else
                {
                    stuck = true;
                }
                /**/ Kiiras(p); /**/
            }

            return gpm(p);
        }

        //algoritmus (Random Restart)
        static string HillClimbingRandomRestart(/* S, ds, Ɛ, f, StopCondition*/)
        {
            /**/ int p = 0; /**/
            List<int> V = new List<int>();

            while (!StopCondition())
            {
                p = RandomElemSminuszVbol(S, V); // p ←rnd- S∖V 
                bool stuck = false;

                while (!stuck && !StopCondition())
                {
                    int q = LegjobbSzamEpszilonTavolsagra(p);

                    if (!V.Contains(q) && f(q) < f(p)) // (q ∉ V) ∧ (...)
                    {
                        p = q;
                        V.Add(q); // V ← V ∪ {q}
                    }
                    else
                    {
                        stuck = true;
                    }
                }
                /**/ Kiiras(p); /**/
            }

            return gpm(p);
        }

        #region belső függvények
        private static int RandomSzamEpszilonTavolsagra(int p)
        {
            List<int> epszilonSugarbanLevoElemek = new List<int>();

            for (int i = 0; i < S.Count; i++)
            {
                if (Ds(i, p) == epszilon && i != p)
                {
                    epszilonSugarbanLevoElemek.Add(i);
                }
            }

            return epszilonSugarbanLevoElemek[R.Next(0, epszilonSugarbanLevoElemek.Count)];
        }

        private static int LegjobbSzamEpszilonTavolsagra(int p)
        {
            List<int> epszilonSugarbanLevoElemek = new List<int>();

            for (int i = 0; i < S.Count; i++)
            {
                if (Ds(i, p) == epszilon && i != p)
                {
                    epszilonSugarbanLevoElemek.Add(i);
                }
            }

            int legjobbElem = epszilonSugarbanLevoElemek[0];

            foreach (var item in epszilonSugarbanLevoElemek)
            {
                if (f(item) < f(legjobbElem))
                {
                    legjobbElem = item;
                }
            }

            return legjobbElem;
        }

        private static string gpm(int p)
        {
            return $"\nLegkisebb elem: index: {p}, érték: {S[p]}";
        }

        private static void Kiiras(int p)
        {
            Console.Write($"\nindex: {p}, érték: {S[p]}");
        }

        private static int RandomElemSminuszVbol(List<int> s, List<int> v)
        {
            List<int> ujS = new List<int>();

            foreach (var item in s)
            {
                if (!v.Contains(item))
                {
                    ujS.Add(item);
                }
            }


            return R.Next(ujS[ujS.Count-1]);
        }
        #endregion

    }
}
