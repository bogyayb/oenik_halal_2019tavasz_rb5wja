﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SzimulaltLehutes
{
    class Program
    {
        #region paraméterek
        static Random R;

        //függvény paraméterei
        static List<int> S;

        static int epszilon;

        static int t_max;

        static int Ds(int p, int x)
        {
            return Math.Abs(p - x);
        }

        private static int f(int ertek)
        {
            return S[ertek];
        }

        static bool StopCondition()
        {
            t++;
            if (t >= t_max)
            {
                return true;
            }
            return false;
        }

        //segéd paraméterek
        static int t;

        //inicializálás
        static void Inicializálás()
        {
            R = new Random();
            S = new List<int>();
            epszilon = 3;
            t_max = 100;
            t = 0;
            //feltöltés 49-től 1-ig
            for (int i = 50; i >= 1; i--)
            {
                S.Add(i);
            }

            //feltöltés 0-től 49-ig
            for (int i = 0; i <= 50; i++)
            {
                S.Add(i);
            }

            ////feltöltés 0-től 49-ig
            //for (int i = 50; i <= 100; i++)
            //{
            //    S.Add(i);
            //}


            ////feltöltés 49-től 1-ig
            //for (int i = 101; i >= 50; i--)
            //{
            //    S.Add(i);
            //}

            ////feltöltés 0-től 49-ig
            //for (int i = 51; i <= 100; i++)
            //{
            //    S.Add(i);
            //}


            //kiírás
            Console.WriteLine("Keresési tér:");
            S.ForEach(x => Console.Write(x + " "));
            Console.WriteLine();
        }
        #endregion

        static void Main(string[] args)
        {
            Inicializálás();
            Console.WriteLine(SimulatedAnnealing(S, 1));
        }

        /// <summary>
        /// t: hányadik iterációnál járunk
        /// kB: Boltzmann állandó = 0.01
        /// Temerature() :  csökkenti a hőmérsékletet(3 féle eljárás is van rá: időkorlátos, idővel lassulva csökken, fitness függő
        /// Popt: az eddigi legjobb elem
        /// ∆E: 'q' és 'p' elem fitness érétkének a különbsége
        /// P: valószínűség(így számítjuk és kész)
        /// RNDu(0, 1) : ???
        /// </summary>
        static string SimulatedAnnealing(List<int> S /* ds, f, Ɛ */, double kB /*, Temperature, StopCondition */ )
        {
            int p = R.Next(S.Count);
            int p_opt = p;
            t = 1;

            while (!StopCondition())
            {
                int q = RandomSzamEpszilonTavolsagra(p, epszilon); //q ←rnd- {x ∈ S | ds(x, p) = Ɛ}
                int deltaE = f(q) - f(p); //pozitív esetben azt mutatja, hogy mennyivel lépnénk rosszabb helyre

                if (deltaE < 0) //Azt mindig megengedjük, hogy egy alacsonyabb energiaszintű́ állapotra lépjünk, azaz jobb helyre
                {
                    p = q;
                    if (f(p) < f(p_opt))
                    {
                        p_opt = p;
                    }
                }
                else
                {
                    double T = Temperature(t); // T elvárási: 0 felé közelítsen és monton csökkenő legyen
                    double P = Math.Pow(Math.E, (-1) * (deltaE / Math.Pow(kB, T))); //kiszámolunk egy valószínűséget

                    if (RNDu(0,1) < P) //ha ez a valószínűségnél kisebb a random számunk, akkor megengedjük a rosszabra irányú lépést is
                    {
                        p = q;
                    }
                }
                /**/ Kiiras(p); /**/
            }

            return gpm(p_opt);
        }

        #region belső függvények
        private static double Temperature(int t)
        {
            //Temperature(t + 1) = Temperature(t) ∗ (1 − ε)
            //Az 0 < ε < 1 param éterrel tudjuk meghataározni, hogy mennyire gyorsan csökkenjen a hoőmérséklet.
            return t_max - t * 3;
        }

        private static int RandomSzamEpszilonTavolsagra(int p, int epszilon)
        {
            List<int> epszilonSugarbanLevoElemek = new List<int>();

            for (int i = 0; i < S.Count; i++)
            {
                if (Ds(i, p) == epszilon && i != p)
                {
                    epszilonSugarbanLevoElemek.Add(i);
                }
            }

            return epszilonSugarbanLevoElemek[R.Next(0, epszilonSugarbanLevoElemek.Count)];
        }

        private static double RNDu(int v1, int v2)
        {
            double r = (double)R.Next(0, 100) / 100;
            return r;
        }

        private static string gpm(int p)
        {
            return $"\nLegkisebb elem: index: {p}, érték: {S[p]}";
        }

        private static void Kiiras(int p)
        {
            Console.Write($"\nindex: {p}, érték: {S[p]}");
        }
        #endregion
    }
}
