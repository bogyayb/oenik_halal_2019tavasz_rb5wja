﻿using System;
using System.Collections.Generic;
using System.Linq;
using CCTUnifor.ConsoleTable;

namespace GenetikusAlgoritmus
{
    class Ember
    {
        public int Magassag { get; set; }
        public int Suly { get; set; }
        public int IQ { get; set; }

        public double Fitness { get; set; }
    }
    
    class Program
    {
        #region paraméterek
        static Random R;

        //függvény paraméterei
        static Ember[] S = new Ember[2];

        private static double f(Ember ember)
        {
            double fitness = ((double)ember.Magassag + (double)ember.Suly + (double)ember.IQ * 2)/3;
            return fitness;
        }

        static bool StopCondition(Ember ember)
        {
            if (f(ember) <= 300)
            {
                return false;
            }
            return true;
        }

        //inicializálás
        static void Inicializálás()
        {
            R = new Random();
            S = new Ember[2];

            S[0] = new Ember() { Magassag = 100, Suly = 30, IQ = 20 }; //lehetséges legrosszab adatok
            S[1] = new Ember() { Magassag = 200, Suly = 150, IQ = 200 }; //lehetséges legjobb adatok
        }
        #endregion

        static void Main(string[] args)
        {
            Inicializálás();
            Console.WriteLine(GeneticAlgorithm());
        }

        /// <summary>
        /// P: populáció (kromoszóma, populáció)
        /// P': az előző populáció legfelső 10%-a (elitizmus)
        /// M: mating pool, lehetséges jővőbeli szülők
        /// c: létrehozott gyerek
        /// N: P (populáció) mérete
        /// f: fitness fv, (súlyozott, pareto rangsro, verseny alapú, megosztó fv.)
        /// selection: elitismb (fintess arányos, versengő, rang alapú, stb.)
        /// </summary>
        private static string GeneticAlgorithm(/*| S, f, StopCondition |*/)
        {
            List<Ember> P = InitializePopulation(S);    /**/ TablaKirajzolas(P, "Kezdeti egyedek:"); /**/
            Evaluation(P /*, f */); //célfüggvények kiszámítása minden egyedre
            Ember P_best = Legjobb(P); // P_best ←argmin(f)- { ∀x ∈ P }

            while (!StopCondition( /**/P_best/**/ ))
            {
                List<Ember> P_vesszo = SelectParents1(P /* f */); //legjobb 10% alaból átkerül a következő populációba --> elitizmus
                List<Ember> M = SelectParents2(P /* f */); //maradék 90% lesz a mating pool, a leendő gyerekek lehetséges szülei

                /**/ int N = P.Count; /**/
                while (P_vesszo.Count < N)
                {
                    //ez egy random kiválasztás, 'k' db elemet választunk ki (most pl. 4-et)
                    List<Ember> parents = Selection(M /* f */); // (p1, p2, ... , pk) ← Selection(M, f)
                    Ember c = CrossOver(parents); // c ← CrossOver(p1, p2, ... , pk)
                    c = Mutate(c);
                    P_vesszo.Add(c); // P' ← P' ∪ {c}
                }

                P = Masolas(P_vesszo);
                Evaluation(P /*, f */);
                P_best = Legjobb(P);
            }

            return gpm(P_best);
        }

        /// <summary>
        /// winCnt: megnyert versengéseket számolja
        /// win: ha egy kihávoson vesztettük → az a versengés veszteséges
        /// t: ennyi darab versengés
        /// c: mindegyikben ennyi db másik egyedet hív ki
        /// Pop: random kiválasztott ellenfelek
        private static double TournamentFitness(Ember p, List<Ember> Pop, int t, int c)
        {
            int winCnt = 0;

            for (int i = 0; i < t; i++)
            {
                bool win = true;
                int j = 1;

                while (win && j <= c)
                {
                    Ember q = Pop[R.Next(0, Pop.Count)]; // q ←rnd- Pop

                    if (f(q) > f(p)) //nagyobb a jobb most
                    {
                        win = false;
                    }

                    j++;
                }

                if (win)
                {
                    winCnt = winCnt + 1;
                }
            }

            return winCnt;
        }

        #region belső függvények
        private static List<Ember> InitializePopulation(Ember[] S)
        {
            List<Ember> P = new List<Ember>();

            for (int i = 0; i < 100; i++)
            {
                P.Add(new Ember { Magassag = R.Next(S[0].Magassag, S[1].Magassag), Suly = R.Next(S[0].Suly, S[1].Suly), IQ = R.Next(S[0].IQ, S[1].IQ) });
            }


            return P;
        }

        private static void Evaluation(List<Ember> P)
        {
            foreach (var item in P)
            {
                List<Ember> Pop = new List<Ember>();
                for (int i = 0; i < 50; i++)
                {
                    Pop.Add(P[R.Next(0, P.Count())]);
                }

                item.Fitness = TournamentFitness(item, Pop, 20, 2);
            }
        }

        private static Ember Legjobb(List<Ember> P)
        {
            Ember legjobb = P[0];

            foreach (var item in P)
            {
                if (item.Fitness > legjobb.Fitness)
                {
                    legjobb = item;
                }
            }

            return legjobb;
        }

        private static List<Ember> SelectParents1(List<Ember> P)
        {
            List<Ember> P_vesszo = new List<Ember>();
            P_vesszo = P.OrderByDescending(x => x.Fitness).Take((P.Count / 100) * 10).ToList();

            return P_vesszo;
        }

        private static List<Ember> SelectParents2(List<Ember> P)
        {
            List<Ember> P_vesszo = new List<Ember>();
            P_vesszo = P.OrderBy(x => x.Fitness).Take((P.Count / 100) * 90).ToList();

            return P_vesszo;
        }

        private static List<Ember> Selection(List<Ember> M)
        {
            List<Ember> parents = new List<Ember>();

            for (int i = 0; i < 4; i++) //most itt 4 db szülő lesz :D 
            {
                parents.Add(M[R.Next(0, M.Count)]);
            }

            return parents;
        }

        private static Ember Mutate(Ember C)
        {
            Ember C_mutalt = C;

            C_mutalt.Magassag += R.Next(0, 5);
            C_mutalt.Suly += R.Next(0, 5);
            C_mutalt.IQ += R.Next(0, 5);

            return C_mutalt;

        }

        private static Ember CrossOver(List<Ember> parents)
        {
            Ember c = new Ember();

            c.Magassag = parents.OrderByDescending(x => x.Magassag).ToList().First().Magassag;
            c.Suly = parents.OrderByDescending(x => x.Suly).ToList().First().Suly;
            c.IQ = parents.OrderByDescending(x => x.IQ).ToList().First().IQ;

            return c;
        }

        private static List<Ember> Masolas(List<Ember> P_vesszo)
        {
            List<Ember> P = new List<Ember>();

            foreach (var item in P_vesszo)
            {
                P.Add(item);
            }

            return P;
        }

        private static string gpm(Ember p_best)
        {
            return $"\n------------------------------\nFelfejlődött egyedünk:\n - Magasság: {p_best.Magassag} cm\n - Súly: {p_best.Suly} Kg\n - IQ: {p_best.IQ}\n - Fitness: {(int)p_best.Fitness}\n------------------------------";
        }

        private static void TablaKirajzolas(List<Ember> P, string cim)
        {
            Console.WriteLine("------------------------------");
            Console.WriteLine(cim);
            Console.WriteLine("------------------------------");
            Evaluation(P);

            string[] szamozas = new string[P.Count];

            for (int i = 0; i < P.Count; i++)
            {
                szamozas[i] = (i + 1).ToString();
            }

            ConsoleTable table = new ConsoleTable(new string[] { "Fitness", "Magasság", "Súly", "IQ" }, szamozas);

            foreach (var item in P.OrderBy(x => x.Fitness))
            {
                table.AddRow(new string[] { ((int)(item.Fitness)).ToString(), item.Magassag.ToString() + " cm", item.Suly.ToString() + " Kg", item.IQ.ToString() });
            }
            table.Write();
        }
        #endregion

    }
}
