﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VeletlenOptimalizalas
{
    class Program
    {
        #region paraméterek
        static Random R;

        //függvény paraméterei
        static List<int> S;

        static int Ds(int p, int x)
        {
            //return Math.Abs(f(p) - f(x));
            return Math.Abs(p - x);

        }

        private static int f(int index)
        {
            return S[index];
        }

        static bool StopCondition()
        {
            stopCounter++;
            if (stopCounter >= 100)
            {
                return true;
            }
            return false;
        }

        //segéd paraméterek
        static int stopCounter;

        //inicializálás
        static void Inicializálás()
        {
            R = new Random();
            S = new List<int>();
            stopCounter = 0;

            //feltöltés 49-től 1-ig
            for (int i = 50; i >= 1; i--)
            {
                S.Add(i);
            }

            //feltöltés 0-től 49-ig
            for (int i = 0; i <= 50; i++)
            {
                S.Add(i);
            }

            //kiírás
            Console.WriteLine("Keresési tér:");
            S.ForEach(x => Console.Write(x + " "));
            Console.WriteLine();
        }
        #endregion

        static void Main(string[] args)
        {
            Inicializálás();
            Console.WriteLine(RandomOptimization());
        }

        /// <summary>
        /// C: megszorítások halmaza
        /// mű: haladási irány(1D-ben pl.: 0/1)
        /// szigma: vltlen szám méretét a lépés nagyságát befolyásolják, ha javul a fitness, akkor ezt lehet csökkenteni és jobban konvergálunk a megoldás felé
        /// step: a lépések minden D-re nézve a normáls eloszlások alapján
        /// </summary>
        static string RandomOptimization(/*| S, f, C, mu?, szigma?, StopCondition |*/)
        {
            int p = R.Next(S.Count);

            while (!StopCondition())
            {
                int step = RandomLepesIranyEsTavolsagMeghatarozasa_RandomMuEsSzigmaAlapjan_MindenDimenzioban(p); // nem egy fix távolságot lépünk, hanem ezt egy (ν, σ) paraméterű normál eloszlással tesszük meg
                int q = p + step;

                if (MindenMegszoritastTeljesitE_q_a_C_halmazban(q)) // ∀c ∈ C | c(q) ≥ 0
                {
                    if (f(q) <= f(p))
                    {
                        p = q;
                    }
                }

                /**/ Kiiras(p); /**/
            }

            return gpm(p);
        }

        #region belső függvények
        private static int RandomLepesIranyEsTavolsagMeghatarozasa_RandomMuEsSzigmaAlapjan_MindenDimenzioban(int p)
        {
            int step = 0;

            int mu = R.Next(2) == 0 ? 1 : -1; //mű - irany
            int maxRand = 5;
            if (mu == 1)
            {
                int maxSeg = S.Count - p;
                if (maxSeg < maxRand)
                {
                    maxRand = maxSeg;
                }
            }
            else
            {             
                int maxSeg = p;
                if (maxSeg < maxRand)
                {
                    maxRand = maxSeg;
                }
            }

            int sigma = R.Next(1, maxRand);
            step = mu * sigma;

            return step;
        }

        private static bool MindenMegszoritastTeljesitE_q_a_C_halmazban(int q)
        {

            bool mindegyikIgazE = false;

            //összes c-re megnézzük a C halmazban (C: megszorítás fv.-ek listája)
            if (C1_ParosE(q))
            {
                mindegyikIgazE = true;
            }

            return mindegyikIgazE;
        }

        private static string gpm(int p)
        {
            return $"\nLegkisebb elem: index: {p}, érték: {S[p]}";
        }

        private static void Kiiras(int p)
        {
            Console.Write($"\nindex: {p}, érték: {S[p]}");
        }

        //megszorítások: c-k
        public static bool C1_ParosE(int x)
        {
            return x % 2 == 0;
        }
        #endregion
    }
}

