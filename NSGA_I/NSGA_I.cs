﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NSGA_I
{
    class Elofizetes
    {
        public int Havidij { get; set; } // ezer Ft
        public int Husegido { get; set; } // év

        public double fintess { get; set; }
        public double sh { get; set; }
    }

    class Program
    {
        #region paraméterek
        static Random R;

        //függvény paraméterei
        static List<Elofizetes> P;

        static List<Elofizetes> F;

        static double Ds(Elofizetes x, Elofizetes y)
        {
            return (Math.Sqrt(Math.Pow(Math.Pow(Math.Abs(x.Havidij - y.Havidij), 2) + Math.Abs(x.Husegido - y.Husegido), 2)));
        }

        //inicializálás

        static int minHusegido = 0;
        static int maxHusegido = 10;
        static int minHavidij = 5;
        static int maxHavidij = 15;

        static void Inicializálás()
        {
            R = new Random();
            P = new List<Elofizetes>();

            for (int i = 0; i < 100; i++)
            {
                P.Add(new Elofizetes { Husegido = R.Next(minHusegido, maxHusegido), Havidij = R.Next(minHavidij, maxHavidij) });
            }

        }
        #endregion

        static void Main(string[] args)
        {
            Inicializálás();
            EredmenyKiiras(NSGA_I(0.9));
        }

        /// <summary>
        /// P: problématér
        /// M: mating pool, Pareto Frontokat tartalmazó lista(listák listája)
        /// F: a jelenlegi Pareto Front, elemeket tartalmazó lista
        /// SH(sharing): eloszlás számító képlet
        /// próbálja szétszórni az elemeket, érzékeny a szigma értékre, nincs benne elitizmus
        /// </summary>
        private static List<List<Elofizetes>> NSGA_kivalasztas(/*| P, ds, Sh, |*/ double fitness_deg)
        {
            List<List<Elofizetes>> M = new List<List<Elofizetes>>(); // M: mating pool
            double fitness_base = 1;

            while (P.Count > 0)
            {
                F = new List<Elofizetes>();

                //pareto front kiválasztása
                foreach (Elofizetes p in P) // ∀p ∈ P
                {
                    if( NemLetezikOlyan_q_Elem_A_P_HalmazbanAkiDominálná_p_t(p) ) // ∄q ∈ P | q ≺ p
                    {
                        F.Add(p); // F ← F ∪ {p}
                    }
                }

                HalmazKivonas(P, F); // P ← P \ F 

                //SH értékek kiszámítás az adott pareto fronton belül mindenkire
                foreach (Elofizetes p in F)
                {
                    p.sh = F.Sum(q => Sh(p, q)); //                                                                         | psh ← ∑ SH(ds(p,q))
                }

                double S_sh = F.Sum(p => p.sh); //sharing biztosítja a szétszórást

                //fitness értékek kiszámítása az adott pareto frontban
                foreach (Elofizetes p in F)
                {
                    /**/ if (S_sh != 0.0) /**/

                    p.fintess = fitness_base * (1 - (p.sh / S_sh)); //nagyobb fitnessze lesz annak, akik távol vannak mindenkitől az adott pareto frtonton belül

                    /**/ else /**/
                    /**/ p.fintess = fitness_base * (1); /**/
                }

                M.Add(F);

                fitness_base = F.Min(x => x.fintess) * fitness_deg; //létrehozunk egy új alap fitness-t, hogy a kövi frontban biztos ne legyenek még csak ugyanakkorák sem, mint az előző front legkisebb fitnessű fitnesse
            }
            return M;
        }

        #region belső függvények
        private static double Sh(Elofizetes i, Elofizetes j)
        {
            double sh = 0;

            //szigma_share: legnagyobb távolság bármely 2 pont között a fronton belül
            double szigma_share = Ds(i, F[0]);

            //szgima_share kiszámítása
            foreach (var item in F)
            {
                if (Ds(i, item) > szigma_share)
                {
                    szigma_share = Ds(i, item);
                }
            }

            //----------------------------------------------------
            //SH(dij) képlet
            //----------------------------------------------------
            if (Ds(i, j) < szigma_share)
            {
                sh = 1 - Math.Pow( (Ds(i, j)/szigma_share) , 2); 
            }
            else
            {
                sh = 0;
            }
            //----------------------------------------------------

            return sh;
        }

        private static List<Elofizetes> Atmasolas(List<Elofizetes> f)
        {
            List<Elofizetes> f_ = new List<Elofizetes>();

            foreach (var item in f)
            {
                f_.Add(item);
            }

            return f_;
        }

        private static void HalmazKivonas(List<Elofizetes> p, List<Elofizetes> f)
        {
            foreach (var item in f)
            {
                if (P.Contains(item))
                {
                    P.Remove(item);
                }
            }
        }

        private static bool NemLetezikOlyan_q_Elem_A_P_HalmazbanAkiDominálná_p_t(Elofizetes p)
        {
            bool vanAkiDominalja = true;

            foreach (Elofizetes q in P)
            {
                if (q.Havidij <= p.Havidij && q.Husegido <= p.Husegido)
                {
                    if (q.Havidij < p.Havidij || q.Husegido < p.Husegido)
                    {
                        vanAkiDominalja = false;
                    }
                }
            }

            return vanAkiDominalja;
        }

        private static void EredmenyKiiras(List<List<Elofizetes>> m)
        {
            Console.WriteLine("Elős Pareto Front:");
            Console.WriteLine("------------------");
            foreach (Elofizetes item in m[0].OrderByDescending(x => x.fintess))
            {
                Console.WriteLine($"Havidíj: {item.Havidij}, Hűségidő {item.Husegido} (fitness: {item.fintess})");
            }
            Console.WriteLine("------------------");
            Console.WriteLine();

            Console.WriteLine("Második Pareto Front:");
            Console.WriteLine("------------------");
            foreach (Elofizetes item in m[1].OrderByDescending(x => x.fintess))
            {
                Console.WriteLine($"Havidíj: {item.Havidij}, Hűségidő {item.Husegido} (fitness: {item.fintess})");
            }
            Console.WriteLine("------------------");
            Console.WriteLine();

            Console.WriteLine("Harmadik Pareto Front:");
            Console.WriteLine("------------------");
            foreach (Elofizetes item in m[2].OrderByDescending(x => x.fintess))
            {
                Console.WriteLine($"Havidíj: {item.Havidij}, Hűségidő {item.Husegido} (fitness: {item.fintess})");
            }
            Console.WriteLine("------------------");
            Console.WriteLine();
        }
        #endregion
    }
}
