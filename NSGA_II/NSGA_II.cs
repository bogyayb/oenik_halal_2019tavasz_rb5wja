﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NSGA_II
{
    class Elofizetes
    {
        public int Havidij { get; set; } // ezer Ft
        public int Husegido { get; set; } // év

        public int np { get; set; } //hanyan dominaljak őt
        public List<Elofizetes> Sp { get; set; } //akiket ő dominal azok halmaza
        public int rank { get; set; }
        public double fintess { get; set; }
        public double sh { get; set; }
        public double dist { get; set; }
    }

    class Program
    {
        static Random r;

        #region függvény paraméterei
        static List<Elofizetes> P;
        static List<Elofizetes> Q; //gyerekek
        static List<Elofizetes> F;

        static double Ds(Elofizetes x, Elofizetes y)
        {
            return (Math.Sqrt(Math.Pow(Math.Pow(Math.Abs(x.Havidij - y.Havidij), 2) + Math.Abs(x.Husegido - y.Husegido), 2)));
        }

        static bool StopCondition()
        {
            foreach (var item in P)
            {
                if (item.rank == 0)
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Inicializálás
        static int minHusegido = 0;
        static int maxHusegido = 10;
        static int minHavidij = 5;
        static int maxHavidij = 15;

        static List<Elofizetes> InitializePopulation()
        {
            r = new Random();
            R = new List<Elofizetes>();
            P = new List<Elofizetes>();
            Q = new List<Elofizetes>();

            for (int i = 0; i < 100; i++)
            {
                P.Add(new Elofizetes { Husegido = r.Next(minHusegido, maxHusegido), Havidij = r.Next(minHavidij, maxHavidij) });
            }

            return P;

        }
        #endregion

        static void Main(string[] args)
        {
            EredmenyKiiras(NSGA_II(), 1);
        }

        /// <summary> | NSGA II. | 
        /// P: populáció
        /// Q: következő populáció, új, létrehozott gyerekekkel együtt
        /// G: célfüggvények halmaza('g'-k)
        /// </summary>
        /// <returns>The ii.</returns>
        private static List<Elofizetes> NSGA_II(/* S, G, StopCondiditon */)
        {
            /**/ List<Elofizetes> P_best = new List<Elofizetes>(); /**/
            P = InitializePopulation(/* S */);
            Q = new List<Elofizetes>();

            while (!StopCondition())
            {
                P = Selection(P, Q /*, G */);
                P_best = P.Where(x => x.rank == 1).ToList(); // P_best ← { x ∈ P | x_rank = 1 }
                Q = MakeNewPopulation(P) /*, ≺ */;
            }
            return P_best;
        }

        /// <summary>
        /// M: mating pool, leendő szülők
        /// R: P és Q együtt, előző generáció(P) + gyerekek(Q)
        /// N: |P|
        /// F: jelenlegi Pareto Front
        /// .dist: adott 'p' elem, CrowdingDistance-e
        /// pfi: hanyadik Pareto Front-ot számoljuk
        /// </summary>
        private static List<Elofizetes> Selection(List<Elofizetes> P, List<Elofizetes> Q /*, G */)
        {
            /**/ int N = P.Count; /**/

            List<Elofizetes> M = new List<Elofizetes>(); // M: mating pool
            R = P.Union(Q).ToList(); // P, Q uniója
            NonDominatedSort(R);
            int pfi = 1; //front index           

            while (M.Count < N)
            {
                F = R.Where(p => p.rank == pfi).ToList(); // F ← { p ∈ R | p_rank = pfi }

                //ha minden elem átfér, akkor berakjuk mindet
                if ( (M.Count + F.Count) <= N)
                {
                    M = M.Union(F).ToList();
                }
                else //ha nem
                {
                    CrowdingDistance(F /*, G */); //akkor corwding distance-t számolunk,
                    Sort(F /* .dist */); //rendezzük

                    //és annyit pkaolunk bele, amennyi belefér
                    for (int i = 0; i < N - M.Count; i++)
                    {
                        M.Add(F[i]);
                    }
                }

                /**/ Console.WriteLine(pfi);/**/

                pfi = pfi + 1;
            }

            return M;
        }

        /// <summary>
        /// np: hány másik elem dominálja a 'p'-t
        /// Sp: a 'p' elem által dominált elemek halmaza
        /// F': következő Pareto Front
        /// </summary>
        private static void NonDominatedSort(List<Elofizetes> R)
        {
            F = new List<Elofizetes>();

            //mindenki érétkei inicializáljuk, nullázzuk
            foreach (Elofizetes p in R)
            {
                p.np = 0;
                p.Sp = new List<Elofizetes>();
            }

            //megszámoljuk h ki kit hányan dominálnak és feltöltjük az általuk domináltak listáját
            foreach (Elofizetes p in R)
            {
                foreach (Elofizetes q in R.Where(x => x != p)) // ∀q ∈ R \ {p}
                {
                    if (Q_dominalja_P_t(q, p))
                    {
                        p.np = p.np + 1;
                        q.Sp.Add(p);
                    }
                }

                //akit nem dominált senki, ők lesznek a jelenlegi Pareto Front
                if (p.np == 0)
                {
                    F.Add(p);
                }
            }

            int pfi = 1;

            while (F.Count != 0) // F ≠ ∅
            {
                /**/ if (F.Count != 0) { EredmenyKiiras(F, pfi); } /**/

                List<Elofizetes> F_vesszo = new List<Elofizetes>();

                //végigmegyünk a jelenlegi pareto fronton
                foreach (Elofizetes p in F)
                {
                    //beállítjuk a 'rank' - jukat
                    p.rank = pfi;

                    //az össze olyan elemen végigmeyünk, akiket ő dominál
                    foreach (Elofizetes q in p.Sp)
                    {
                        //az általuk domináltaknál mind csökkentjük az őket dominálók számát
                        q.np = q.np - 1; 

                        //így ha 1 volt mostmár 0 lesz, azaz ők lesznek ak övi Pareto Front
                        if (q.np == 0)
                        {
                            F_vesszo.Add(q);
                        }
                    }
                }

                pfi = pfi + 1;
                F = HalmazMasolas(F_vesszo);
            }

        }

        /// <summary>
        /// pdist: adott 'p' elemt CrowdingDistance értéke
        /// gm: 'm'-edik 'g' (célfüggvény)
        /// F[1] dist: 'm'-edik 'g' alapján a legjobb elem az adott ParetoFront-ban(PF)
        /// F[| F |] dist: 'm'-edik 'g' alapján a legrosszabb elem az adott ParetoFront-ban
        /// gm(gpm(F[i])) : a PF-ben az 'i'-edik elem 'm'-edik 'g' alapján számítot értéke
        /// gmmax: az 'm'-edik 'g' alapján a legjobb elem értéke az adott PF-ban
        /// gmmin: az 'm'-edik 'g' alapján a legrosszabb elem értéke az adott PF-ban
        /// </summary>
        private static void CrowdingDistance(List<Elofizetes> F /*, G */)
        {
            //mindenki ditance-ét nullázzuk, mert újraszámoljuk, az adott pareto frontban
            foreach (Elofizetes p in F)
            {
                p.dist = 0;
            }

            //ciklus ∀ gm ∈ G

              //elso ciklus
                Sort(F, "havidij"); // havidij = gm
                F[0].dist = int.MaxValue;
                F[F.Count-1].dist = int.MaxValue;

                for (int i = 1; i < F.Count-2; i++)
                {
                    /**/ int gm_max = F[0].Havidij; /**/
                    /**/ int gm_min = F[F.Count - 1].Havidij; /**/

                    F[i].dist = F[i].dist + ( F[i+1].Havidij - F[i - 1].Havidij / gm_max  - gm_min); // F[i+1].Havidij = gm(gpm(F[i+1])) = havidij(gpm(F[i+1]))
                }   
              //ciklus vége

              //masodik ciklus
                Sort(F, "husegido"); // adott 'g' (célfüggvény) alapján rendezünk
                F[0].dist = int.MaxValue;
                F[F.Count - 1].dist = int.MaxValue;

                for (int i = 1; i < F.Count - 2; i++)
                {
                    int gm_max = F[0].Husegido;
                    int gm_min = F[F.Count - 1].Husegido;

                    F[i].dist = F[i].dist + (F[i + 1].Husegido - F[i - 1].Husegido / gm_max - gm_min); //hozzáadjuk az elem dist-jéhez az adott 'g', a 2 szomszédos elem, valamint a min. és max. értékek segítségével kiszámított normalizált értéket
            }
              //ciklus vége

            //ciklus vége
        }


        #region belső függvények

        private static List<Elofizetes> MakeNewPopulation(List<Elofizetes> p)
        {
            return P.OrderBy(x => x.rank).ToList().Take(10).ToList();
        }

        private static List<Elofizetes> HalmazMasolas(List<Elofizetes> f_vesszo)
        {
            List<Elofizetes> uj = new List<Elofizetes>();
            foreach (var item in f_vesszo)
            {
                uj.Add(item);
            }
            return uj;
        }

        private static bool Q_dominalja_P_t(Elofizetes q, Elofizetes p)
        {
            bool dominalja = false;

            if (q.Havidij <= p.Havidij && q.Husegido <= p.Husegido)
            {
                if (q.Havidij < p.Havidij || q.Husegido < p.Husegido)
                {
                    dominalja = true;
                }
            }

            return dominalja;
        }


        private static void Sort(List<Elofizetes> F)
        {
            foreach (var p in F)
            {
                p.dist = F.Sum(q => Sh(p, q));
            }

            F = F.OrderBy(x => x.dist).ToList();
        }

        private static void Sort(List<Elofizetes> F, string miAlapjan)
        {
            foreach (var p in F)
            {
                p.dist = F.Sum(q => Sh(p, q));
            }

            if (miAlapjan == "havidij")
            {
                F = F.OrderBy(x => x.Havidij).ToList();
            }
            else
                F = F.OrderBy(x => x.Husegido).ToList();

        }

        private static List<Elofizetes> HalmazÖsszeadás(List<Elofizetes> p, List<Elofizetes> q)
        {
            List<Elofizetes> uj = new List<Elofizetes>();

            foreach (var item in p)
            {
                uj.Add(item);
            }

            foreach (var item in q)
            {
                uj.Add(item);
            }

            return uj;
        }
        static List<Elofizetes> R;

        private static double Sh(Elofizetes p, Elofizetes q)
        {
            double sh = 0;

            //szigma számítás
            //List<Elofizetes> F_kereses = Atmasolas(F);
            //F_kereses.Remove(p);

            double szigma = Ds(p, F[0]);

            foreach (var item in F)
            {
                if (Ds(p, item) > szigma)
                {
                    szigma = Ds(p, item);
                }
            }

            //eldöntés
            if (Ds(p, q) < szigma)
            {
                sh = 1 - Math.Pow((Ds(p, q) / szigma), 2);
            }
            else
            {
                sh = 0;
            }

            return sh;
        }

        private static List<Elofizetes> Atmasolas(List<Elofizetes> f)
        {
            List<Elofizetes> f_ = new List<Elofizetes>();

            foreach (var item in f)
            {
                f_.Add(item);
            }

            return f_;
        }

        private static void HalmazKivonas(List<Elofizetes> p, List<Elofizetes> f)
        {
            foreach (var item in f)
            {
                if (P.Contains(item))
                {
                    P.Remove(item);
                }
            }
        }

        private static bool NemLetezikOlyan_q_Elem_A_P_HalmazbanAkiDominálná_p_t(Elofizetes p)
        {
            bool vanAkiDominalja = true;

            foreach (Elofizetes q in P)
            {
                if (q.Havidij <= p.Havidij && q.Husegido <= p.Husegido)
                {
                    if (q.Havidij < p.Havidij || q.Husegido < p.Husegido)
                    {
                        vanAkiDominalja = false;
                    }
                }
            }

            return vanAkiDominalja;
        }



        private static void EredmenyKiiras(List<Elofizetes> m, int pfi)
        {
            Console.WriteLine("----------------------");
            Console.WriteLine($"{pfi}. Pareto Front:");
            Console.WriteLine("----------------------");
            foreach (Elofizetes item in m.OrderByDescending(x => x.fintess))
            {
                Console.WriteLine($"Havidíj: {item.Havidij}, Hűségidő {item.Husegido}");
            }
            Console.WriteLine("----------------------");
            Console.WriteLine();

        }
        #endregion
    }
}

