﻿using System;
using System.Collections.Generic;

namespace TabuKereses
{
    class Program
    {
        #region paraméterek
        static Random R;

        //függvény paraméterei
        static List<int> S;

        static int epszilon;

        static int Ds(int p, int x)
        {
            return Math.Abs(p - x);
        }

        private static int f(int? index)
        {
            return S[(int)index];
        }

        static bool StopCondition()
        {
            stopCounter++;
            if (stopCounter >= 100)
            {
                return true;
            }
            return false;
        }

        //segéd paraméterek
        static int stopCounter;

        //inicializálás
        static void Inicializálás()
        {
            R = new Random();
            S = new List<int>();
            Tabuk = new List<int>();
            epszilon = 3;
            stopCounter = 0;

            //feltöltés 49-től 1-ig
            for (int i = 50; i >= 1; i--)
            {
                S.Add(i);
            }

            //feltöltés 0-től 49-ig
            for (int i = 0; i <= 50; i++)
            {
                S.Add(i);
            }

            //kiírás
            Console.WriteLine("Keresési tér:");
            S.ForEach(x => Console.Write(x + " "));
            Console.WriteLine();
        }
        #endregion

        static void Main(string[] args)
        {
            Inicializálás();
            Console.WriteLine(TabuSearch());
        }

        //algoritmus
        static string TabuSearch(/* S, Ds, Ɛ, f, StopCondition */)
        {
            /**/ int p = 0; /**/
            int? p_opt = null;

            while (!StopCondition())
            {
                p = R.Next(S.Count); // p ←rnd- S
                SetTabuBarrier(); //jelzések elhelyezése, hogy tudjuk, mik az új pontok az adott keresésben (de, h ezt mikor hasznaljuk azt nemtudom)
                bool stuck = false;

                while (!stuck && !IsTabu(p) && !StopCondition()) //tabu keresés: ineáris indexelés, térbeli inexelés
                {
                    if (p_opt == null || f(p) < f(p_opt)) 
                    {
                        p_opt = p;
                    }
                    
                    AddTabu(p); //végtelen lista, hasító táblák, tulajdonságok tárolása, változók tárolása
                    PurgeTabu(); //FIFO vagy klaszterezés: tömörítés (akkor van rá szükség, ha nincs megfelelő méretű adatbázis tárhelyünk, akkor szelektálunk, pl. klaszterezéssel)

                    int q = LegjobbSzamEpszilonTavolsagra(p); // q ←argmin(f)- {x ∈ S | ds(x, p) = Ɛ}

                    if (f(q) <= f(p))
                    {
                        p = q;
                    }
                    else
                    {
                        stuck = true;
                    }
                }
                /**/ Kiiras(p); /**/
            }

            return gpm(p_opt);
        }

        #region belső függvények
        static List<int> Tabuk;

        private static bool IsTabu(int p)
        {
            foreach (var item in Tabuk)
            {
                if (item == p)
                {
                    return true;
                }
            }

            return false;
        }

        private static void AddTabu(int p)
        {
            Tabuk.Add(p);
        }

        private static void PurgeTabu()
        {
            if (Tabuk.Count >= 30)
            {
                Tabuk.RemoveAt(Tabuk.Count-1);
            }
        }

        private static int LegjobbSzamEpszilonTavolsagra(int p)
        {
            List<int> epszilonSugarbanLevoElemek = new List<int>();

            for (int i = 0; i < S.Count; i++)
            {
                if (Ds(i, p) == epszilon && i != p)
                {
                    epszilonSugarbanLevoElemek.Add(i);
                }
            }

            int legjobbElem = epszilonSugarbanLevoElemek[0];

            foreach (var item in epszilonSugarbanLevoElemek)
            {
                if (f(item) < f(legjobbElem))
                {
                    legjobbElem = item;
                }
            }

            return legjobbElem;
        }

        private static string gpm(int? p)
        {
            return $"\nLegkisebb elem: index: {p}, érték: {S[(int)p]}";
        }

        private static void SetTabuBarrier()
        {
            //####
            foreach (var item in S)
            {
                if (item == S[R.Next(0, 10)])
                {
                    var asd = item;
                }
            }
        }

        private static void Kiiras(int? p)
        {
            Console.Write($"\nindex: {p}, érték: {S[(int)p]}");
        }
        #endregion
    }
}
